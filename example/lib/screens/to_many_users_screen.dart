import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:stripe_example/widgets/loading_button.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class ToManyUsersScreen extends StatefulWidget {
  const ToManyUsersScreen({Key? key}) : super(key: key);

  @override
  _ToManyUsersScreenState createState() => _ToManyUsersScreenState();
}

class _ToManyUsersScreenState extends State<ToManyUsersScreen> {
  final TextEditingController _amountController = TextEditingController();

  late Size _screenSize;

  final TextStyle _headingsTextStyle = TextStyle(
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.symmetric(
            vertical: _screenSize.height * 0.05,
            horizontal: _screenSize.width * 0.1),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: _screenSize.height * 0.03),
              Text(
                'Dispersion amount',
                style: _headingsTextStyle.copyWith(
                    fontSize: _screenSize.height * 0.02),
              ),
              TextField(
                controller: _amountController,
                keyboardType: TextInputType.number,
                onChanged: (v) {
                  setState(() {});
                },
                decoration: InputDecoration(labelText: 'Dispersion amount'),
              ),
              SizedBox(
                height: _screenSize.height * 0.05,
              ),
              Center(
                child: LoadingButton(
                  onPressed:
                      _amountController.text != '' ? _handlePayPress : null,
                  text: 'Dispersion amount',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _handlePayPress() async {
    try {
      // 1. Create payment method
      final url = Uri.parse('$kApiUrl/pay-to-many');
      print(url);
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'amount': _amountController.text,
          'transferUsers': [
            'acct_1J8QnLRA13d3DeYx',
            'acct_1J8R5qRNXHKDn5xd',
            'acct_1J8R7hRF4B05slqE',
            'acct_1J8RFzRQAq2ZVZX3'
          ],
        }),
      );
      return json.decode(response.body);
    } catch (e) {
      print('error');
      print(e);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error: $e')));
    }
  }
}
