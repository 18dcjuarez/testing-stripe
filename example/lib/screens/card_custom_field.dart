import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:stripe_platform_interface/stripe_platform_interface.dart';

/// Customizable form that collects card information.
class CardCustomField extends StatefulWidget {
  const CardCustomField({
    required this.onCardChanged,
    Key? key,
    this.decoration,
    this.enablePostalCode = false,
    this.style,
    this.cursorColor,
    this.numberHintText,
    this.expirationHintText,
    this.cvcHintText,
    this.postalCodeHintText,
  }) : super(key: key);

  /// Decoration related to the input fields.
  final InputDecoration? decoration;

  /// Callback that will be executed when the card information changes.
  final CardChangedCallback onCardChanged;

  /// Textstyle of the card input fields.
  final TextStyle? style;

  /// Color of the cursor when a field gets focus.
  final Color? cursorColor;

  /// Whether or not to show the postalcode field in the form.
  /// Defaults is `false`.
  final bool enablePostalCode;

  /// Hint text for the card number field.
  final String? numberHintText;

  /// Hint text for the expiration date field.
  final String? expirationHintText;

  /// Hint text for the cvc field.
  final String? cvcHintText;

  /// Hint text for the postal code field.
  final String? postalCodeHintText;

  @override
  _CardCustomFieldState createState() => _CardCustomFieldState();
}

class _CardCustomFieldState extends State<CardCustomField> {
  void updateState() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final inputDecoration = effectiveDecoration(widget.decoration);
    final style = effectiveCardStyle(inputDecoration);

    // Arbitrary values compared for both Android and iOS platform
    // For adding a framework input decorator, the platform one is removed
    // together with the extra padding
    final platformCardHeight = style.fontSize! + 31;
    const platformMargin = EdgeInsets.fromLTRB(12, 10, 10, 12);

    final cardHeight = platformCardHeight - platformMargin.vertical;
    return InputDecorator(
      decoration: inputDecoration,
      baseStyle: widget.style,
      child: SizedBox(
        height: cardHeight,
        child: CustomSingleChildLayout(
          delegate: const _NegativeMarginLayout(margin: platformMargin),
          child: _MethodChannelCardField(
            height: platformCardHeight,
            style: style,
            placeholder: CardPlaceholder(
              number: widget.numberHintText,
              expiration: widget.expirationHintText,
              cvc: widget.cvcHintText,
              postalCode: widget.postalCodeHintText,
            ),
            enablePostalCode: widget.enablePostalCode,
            onCardChanged: widget.onCardChanged,
          ),
        ),
      ),
    );
  }

  InputDecoration effectiveDecoration(InputDecoration? decoration) {
    final theme = Theme.of(context).inputDecorationTheme;
    final cardDecoration = decoration ?? const InputDecoration();
    return cardDecoration.applyDefaults(theme);
  }

  CardStyle effectiveCardStyle(InputDecoration decoration) {
    final fontSize = widget.style?.fontSize ??
        Theme.of(context).textTheme.subtitle1?.fontSize ??
        kCardFieldDefaultFontSize;
    return CardStyle(
      textColor: widget.style?.color,
      fontSize: fontSize,
      cursorColor: widget.cursorColor,
      textErrorColor: decoration.errorStyle?.color,
      placeholderColor: decoration.hintStyle?.color,
    );
  }
}

// Crops a view by a given negative margin values.
// http://ln.hixie.ch/?start=1515099369&count=1
class _NegativeMarginLayout extends SingleChildLayoutDelegate {
  const _NegativeMarginLayout({required this.margin});

  final EdgeInsets margin;

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    final biggest = super.getConstraintsForChild(constraints).biggest;
    return BoxConstraints.expand(
      width: biggest.width + margin.horizontal,
      height: biggest.height + margin.vertical,
    );
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    return super.getPositionForChild(size, childSize) - margin.topLeft;
  }

  @override
  bool shouldRelayout(covariant _NegativeMarginLayout oldDelegate) {
    return margin != oldDelegate.margin;
  }
}

class _MethodChannelCardField extends StatefulWidget {
  _MethodChannelCardField({
    required this.onCardChanged,
    Key? key,
    this.style,
    this.placeholder,
    this.enablePostalCode = false,
    double? width,
    double? height = kCardFieldDefaultHeight,
    BoxConstraints? constraints,
  })  : assert(constraints == null || constraints.debugAssertIsValid()),
        constraints = (width != null || height != null)
            ? constraints?.tighten(width: width, height: height) ??
                BoxConstraints.tightFor(width: width, height: height)
            : constraints,
        super(key: key);

  final BoxConstraints? constraints;
  final CardChangedCallback onCardChanged;
  final CardStyle? style;
  final CardPlaceholder? placeholder;
  final bool enablePostalCode;

  // This is used in the platform side to register the view.
  static const _viewType = 'flutter.stripe/card_field';

  // By platform level limitations only one CardField is allowed at the same
  // time.
  // A unique key is used to throw an expection before multiple platform
  // views are created
  static late final _key = UniqueKey();

  @override
  _MethodChannelCardFieldState createState() => _MethodChannelCardFieldState();
}

class _MethodChannelCardFieldState extends State<_MethodChannelCardField> {
  MethodChannel? _methodChannel;
  CardStyle? _lastStyle;
  CardStyle resolveStyle(CardStyle? style) {
    final theme = Theme.of(context);
    final baseTextStyle = Theme.of(context).textTheme.subtitle1;
    return CardStyle(
      borderWidth: 0,
      backgroundColor: Colors.transparent,
      borderColor: Colors.transparent,
      borderRadius: 0,
      cursorColor: theme.textSelectionTheme.cursorColor ?? theme.primaryColor,
      textColor: style?.textColor ??
          baseTextStyle?.color ??
          kCardFieldDefaultTextColor,
      fontSize: baseTextStyle?.fontSize ?? kCardFieldDefaultFontSize,
      textErrorColor:
          theme.inputDecorationTheme.errorStyle?.color ?? theme.errorColor,
      placeholderColor:
          theme.inputDecorationTheme.hintStyle?.color ?? theme.hintColor,
    ).apply(style);
  }

  CardPlaceholder? _lastPlaceholder;
  CardPlaceholder resolvePlaceholder(CardPlaceholder? placeholder) =>
      CardPlaceholder(
        number: '1234123412341234',
        expiration: 'MM/YY',
        cvc: 'CVC',
      ).apply(placeholder);

  @override
  Widget build(BuildContext context) {
    final style = resolveStyle(widget.style);
    final placeholder = resolvePlaceholder(widget.placeholder);
    // Pass parameters to the platform side.
    final creationParams = <String, dynamic>{
      'cardStyle': style.toJson(),
      'placeholder': placeholder.toJson(),
      'postalCodeEnabled': widget.enablePostalCode,
    };

    Widget platform;
    if (defaultTargetPlatform == TargetPlatform.android) {
      platform = _AndroidCardField(
        key: _MethodChannelCardField._key,
        viewType: _MethodChannelCardField._viewType,
        creationParams: creationParams,
        onPlatformViewCreated: onPlatformViewCreated,
      );
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      platform = _UiKitCardField(
        key: _MethodChannelCardField._key,
        viewType: _MethodChannelCardField._viewType,
        creationParams: creationParams,
        onPlatformViewCreated: onPlatformViewCreated,
      );
    } else {
      throw UnsupportedError('Unsupported platform view');
    }
    final constraints = widget.constraints ??
        const BoxConstraints.expand(height: kCardFieldDefaultHeight);

    return Listener(
      onPointerDown: (_) {},
      child: ConstrainedBox(
        constraints: constraints,
        child: platform,
      ),
    );
  }

  @override
  void didChangeDependencies() {
    _lastStyle ??= resolveStyle(widget.style);
    final style = resolveStyle(widget.style);
    if (style != _lastStyle) {
      _methodChannel?.invokeMethod('onStyleChanged', {
        'cardStyle': style.toJson(),
      });
    }
    _lastStyle = style;
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant _MethodChannelCardField oldWidget) {
    if (widget.enablePostalCode != oldWidget.enablePostalCode) {
      _methodChannel?.invokeMethod('onPostalCodeEnabledChanged', {
        'postalCodeEnabled': widget.enablePostalCode,
      });
    }
    _lastStyle ??= resolveStyle(oldWidget.style);
    final style = resolveStyle(widget.style);
    if (style != _lastStyle) {
      _methodChannel?.invokeMethod('onStyleChanged', {
        'cardStyle': style.toJson(),
      });
    }
    _lastStyle = style;
    final placeholder = resolvePlaceholder(widget.placeholder);
    if (placeholder != _lastPlaceholder) {
      _methodChannel?.invokeMethod('onPlaceholderChanged', {
        'placeholder': placeholder.toJson(),
      });
    }
    _lastPlaceholder = placeholder;
    super.didUpdateWidget(oldWidget);
  }

  void onPlatformViewCreated(int viewId) {
    _methodChannel = MethodChannel('flutter.stripe/card_field/$viewId');
    _methodChannel?.setMethodCallHandler((call) async {
      if (call.method == 'onCardChange') {
        _handleCardChanged(call.arguments);
      }
    });
  }

  void _handleCardChanged(arguments) {
    try {
      final map = Map<String, dynamic>.from(arguments);
      final details = CardFieldInputDetails.fromJson(map);
      widget.onCardChanged.call(details);
      // ignore: avoid_catches_without_on_clauses
    } catch (e) {
      log('An error ocurred while while parsing card arguments, this should not happen, please consider creating an issue at https://github.com/flutter-stripe/flutter_stripe/issues/new');
      rethrow;
    }
  }
}

class _AndroidCardField extends StatelessWidget {
  const _AndroidCardField({
    Key? key,
    required this.viewType,
    required this.creationParams,
    required this.onPlatformViewCreated,
  }) : super(key: key);

  final String viewType;
  final Map<String, dynamic> creationParams;
  final PlatformViewCreatedCallback onPlatformViewCreated;

  @override
  Widget build(BuildContext context) {
    return PlatformViewLink(
      viewType: viewType,
      surfaceFactory: (context, controller) => AndroidViewSurface(
        controller: controller
            // ignore: avoid_as
            as AndroidViewController,
        gestureRecognizers: const <Factory<OneSequenceGestureRecognizer>>{},
        hitTestBehavior: PlatformViewHitTestBehavior.opaque,
      ),
      onCreatePlatformView: (params) {
        onPlatformViewCreated(params.id);
        return PlatformViewsService.initSurfaceAndroidView(
          id: params.id,
          viewType: viewType,
          layoutDirection: Directionality.of(context),
          creationParams: creationParams,
          creationParamsCodec: const StandardMessageCodec(),
        )
          ..addOnPlatformViewCreatedListener(params.onPlatformViewCreated)
          ..create();
      },
    );
  }
}

class _UiKitCardField extends StatelessWidget {
  const _UiKitCardField({
    Key? key,
    required this.viewType,
    required this.creationParams,
    required this.onPlatformViewCreated,
  }) : super(key: key);

  final String viewType;
  final Map<String, dynamic> creationParams;
  final PlatformViewCreatedCallback onPlatformViewCreated;

  @override
  Widget build(BuildContext context) {
    return UiKitView(
      viewType: viewType,
      creationParamsCodec: const StandardMessageCodec(),
      creationParams: creationParams,
      onPlatformViewCreated: onPlatformViewCreated,
    );
  }
}

const kCardFieldDefaultHeight = 48.0;
const kCardFieldDefaultFontSize = 17.0;
const kCardFieldDefaultTextColor = Colors.black;

typedef CardChangedCallback = void Function(CardFieldInputDetails? details);
typedef CardFocusCallback = void Function(CardFieldName? focusedField);
