import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:stripe_example/widgets/loading_button.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class OneToOneScreen extends StatefulWidget {
  const OneToOneScreen({Key? key}) : super(key: key);

  @override
  _OneToOneScreenState createState() => _OneToOneScreenState();
}

class _OneToOneScreenState extends State<OneToOneScreen> {
  final TextEditingController _amountController = TextEditingController();

  late Size _screenSize;

  final TextStyle _headingsTextStyle = TextStyle(
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.symmetric(
            vertical: _screenSize.height * 0.05,
            horizontal: _screenSize.width * 0.1),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: _screenSize.height * 0.03),
              Text(
                'Pay Amount',
                style: _headingsTextStyle.copyWith(
                    fontSize: _screenSize.height * 0.02),
              ),
              TextField(
                controller: _amountController,
                keyboardType: TextInputType.number,
                onChanged: (v) {
                  setState(() {});
                },
                decoration: InputDecoration(labelText: 'Amount'),
              ),
              SizedBox(
                height: _screenSize.height * 0.05,
              ),
              Center(
                child: LoadingButton(
                  onPressed:
                      _amountController.text != '' ? _handlePayPress : null,
                  text: 'Make payment',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _handlePayPress() async {
    try {
      // 1. Create payment method
      final url = Uri.parse('$kApiUrl/pay-one-to-one');
      print(url);
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'amount': _amountController.text,
          'currency': 'usd',
          'customer': 'cus_JlzIJdFeOXKJdL', // paying customer
          'paymentMethod':
              'pm_1J8RKLET5KeIrV1cupfyZpTW', // paying customer payment method
          'payTo': 'acct_1J8RLKROtAlPv4Dr', // account to transfer funds
        }),
      );
      return json.decode(response.body);
    } catch (e) {
      print('error');
      print(e);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error: $e')));
    }
  }
}
