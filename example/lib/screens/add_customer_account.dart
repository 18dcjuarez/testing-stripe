import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:stripe_example/screens/card_custom_field.dart';
import 'package:stripe_example/widgets/loading_button.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class AddCustomerScreen extends StatefulWidget {
  const AddCustomerScreen({Key? key}) : super(key: key);

  @override
  _AddCustomerScreenState createState() => _AddCustomerScreenState();
}

class _AddCustomerScreenState extends State<AddCustomerScreen> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  late Size _screenSize;
  CardFieldInputDetails? _card;

  final TextStyle _headingsTextStyle = TextStyle(
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.symmetric(
            vertical: _screenSize.height * 0.05,
            horizontal: _screenSize.width * 0.1),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Card Info',
                  style: _headingsTextStyle.copyWith(
                      fontSize: _screenSize.height * 0.02)),
              CardCustomField(
                onCardChanged: (card) {
                  setState(() {
                    _card = card;
                  });
                },
              ),
              SizedBox(height: _screenSize.height * 0.03),
              Text(
                'Personal Info',
                style: _headingsTextStyle.copyWith(
                    fontSize: _screenSize.height * 0.02),
              ),
              TextField(
                controller: _nameController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(labelText: 'Name'),
              ),
              TextField(
                controller: _emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(labelText: 'Email'),
              ),
              TextField(
                keyboardType: TextInputType.phone,
                controller: _phoneController,
                decoration: InputDecoration(labelText: 'Phone'),
              ),
              SizedBox(
                height: _screenSize.height * 0.05,
              ),
              Center(
                child: LoadingButton(
                  onPressed: _card?.complete == true ? _handlePayPress : null,
                  text: 'Add Customer',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _handlePayPress() async {
    if (_card == null) {
      return;
    }

    try {
      // 1. Gather customer billing information (ex. email)
      final billingDetails = BillingDetails(
        name: _nameController.text,
        email: _emailController.text,
        phone: _phoneController.text,
      );

      // 2. Create payment method
      final paymentMethod =
          await Stripe.instance.createPaymentMethod(PaymentMethodParams.card(
        billingDetails: billingDetails,
      ));
      print('CREATE CUSTOMER ==== PAYMENT METHOD');
      print(paymentMethod.id);
      final url = Uri.parse('$kApiUrl/create-customer');
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'paymentMethod': paymentMethod.id,
          'customerInfo': billingDetails,
          'accountNumber': '000123456789',
          'routingNumber': '110000000',
        }),
      );
      return json.decode(response.body);
    } catch (e) {
      print('error');
      print(e);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error: $e')));
    }
  }
}
